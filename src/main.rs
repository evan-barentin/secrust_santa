mod config;
mod mail;
mod mailer;
mod options;
mod participant;

use self::{config::Config, options::Options};
use anyhow::Result;
use mail::Mail;
use mailer::Mailer;

#[tokio::main]
async fn main() -> Result<()> {
    let options = Options::new();
    let config = Config::new()?;

    let participants = config.participants();

    let mails = {
        let mut mails = vec![];
        for (from, to) in participants
            .iter()
            .zip(participants.iter().cycle().skip(1))
            .take(participants.len() + 1)
        {
            mails.push(Mail::new(
                from,
                to,
                config.mail(),
                config.name(),
                config.address(),
            )?);
        }
        mails
    };

    if options.dry_run {
        println!("All good, Let's get schwifty");
    } else {
        let mailer = Mailer::new(
            options.smtp_login,
            options.smtp_password,
            options.smtp_host,
            options.smtp_port,
        )?;
        for mail in mails {
            mailer.send(mail).await?;
        }
    }

    Ok(())
}
