use super::participant::Participants;
use config::{ConfigError, File};
use serde::Deserialize;

static MAIL: &'static str = r#"
You! Yes you <FROM>!

You should great <TO> with present for this Secret Santa!

XXX
"#;

#[derive(Debug, Deserialize)]
pub struct Config {
    name: String,
    address: String,
    mail: String,
    participants: Participants,
}

impl Config {
    pub fn new() -> Result<Self, ConfigError> {
        let mut config: Config = config::Config::builder()
            .set_default("from", "Secret Santa")?
            .set_default("mail", MAIL)?
            .add_source(File::with_name("config").required(true))
            .build()?
            .try_deserialize()?;

        config.participants.shuffle();

        Ok(config)
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn address(&self) -> &String {
        &self.address
    }

    pub fn mail(&self) -> &String {
        &self.mail
    }

    pub fn participants(&self) -> &Participants {
        &self.participants
    }
}
