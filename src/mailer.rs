use crate::Mail;
use anyhow::{anyhow, Ok, Result};
use lettre::{
    transport::smtp::{authentication::Credentials, response::Response},
    AsyncSmtpTransport, AsyncTransport, Tokio1Executor,
};
use std::ops::Deref;

pub type Transport = AsyncSmtpTransport<Tokio1Executor>;

pub struct Mailer(Transport);

impl Deref for Mailer {
    type Target = Transport;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Mailer {
    pub fn new(
        smtp_login: String,
        smtp_password: String,
        smtp_host: String,
        smtp_port: u16,
    ) -> Result<Self> {
        let transport = Transport::relay(&smtp_host)?
            .credentials(Credentials::new(smtp_login, smtp_password))
            .port(smtp_port)
            .build();
        Ok(Self(transport))
    }

    pub async fn send(&self, mail: Mail) -> Result<Response> {
        self.0
            .send(mail.take())
            .await
            .map_err(|err| anyhow!("Failed to send email {err}"))
    }
}
